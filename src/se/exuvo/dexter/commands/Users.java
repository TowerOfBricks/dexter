package se.exuvo.dexter.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import se.exuvo.dexter.Users.User;
import se.exuvo.dexter.commands.Dexter.P;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;



public class Users extends Command {
	
	public Users(){
		super("getIDs", "");
	}

	@Override
	public int execute(String[] tokens, String phrase) {
		if(tokens.length > 1){
			if(tokens[1].equals("update")){
			    try {
			    	WebClient webClient;
					HtmlPage page;
					{
						P p = Dexter.login();
				    	webClient = p.webClient;
				    	page = p.page;
					}
				    
				    page = webClient.getPage("http://dexter.dagy.danderyd.se/Default.asp?page=gy/bas/schedule_studentsearchpage&show_result=yes&unitid=&StudentId=");
				    
				    Iterable<?> nodes = ((HtmlElement) page.getByXPath("/html/body/table/tbody/tr[3]/td/table/tbody/tr[2]/td[2]/form/table/tbody").get(0)).getChildElements();
				    
				    Iterator<?> it=nodes.iterator();
				    it.next();it.next();it.next();it.next();
				    
				    List<User> userIds = new ArrayList<User>(1600);
				    
				    while(it.hasNext()){
				    	HtmlElement e = (HtmlElement) it.next();
			    		e = (HtmlElement) e.getElementsByTagName("td").get(0);
			    		e = (HtmlElement) e.getElementsByTagName("a").get(0);
			    		
			    		Pattern p = Pattern.compile("(?<=studentid=)[0-9]+(?=\\&)");
			    		Matcher m = p.matcher(e.getAttribute("href"));
			    		
		
			    		if (m.find()) {
			    			User u = new User();
			    			u.id = new Integer(m.group(0));
			    			p = Pattern.compile("^(.*), (.*),[0-9]*,(.*)$");
			    			m = p.matcher(e.getNodeValue());
			    			u.klass = m.group(3);
			    			u.name = m.group(2);
			    			u.lastname = m.group(1);
			    			u.username = u.username();
			    			userIds.add(u);
			    		}
				    }
				    
//				    f(userIds, "userIDs.txt");
			    }catch(Throwable t){
			    	t.printStackTrace();
			    	return 2;
			    }
			}
		}
		help();
		return 0;
	}
	    
    static void f(List<String> l, String name){
    	Dexter.f(l, name);
    }

}
