package se.exuvo.dexter.commands;

import se.exuvo.dexter.Init;

public class Quit extends Command {
	
	public Quit(){
		super("quit", "");
		addName("exit");
	}

	@Override
	public int execute(String[] tokens, String phrase) {
		log.fatal("### Exiting ###");
		System.exit(0);
		return 0;
	}

}
