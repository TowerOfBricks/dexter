package se.exuvo.dexter.commands;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.ThreadedRefreshHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlButtonInput;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;



public class Dexter extends Command {
	private final static String DEFAULT_USERNAME = "";
	private final static String DEFAULT_PASSWORD = "";
	
	
	public Dexter(){
		super("dexter", "dexter username startTime stopTime [date]");
	}

	
//http://dexter.dagy.danderyd.se/Default.asp?page=gy/bas/classaddresslist_searchpage
//http://dexter.dagy.danderyd.se/Default.asp?page=gy/bas/schedule_studentsearchpage
//http://dexter.dagy.danderyd.se/Default.asp?page=gy/devdisc/devdisc_overview$print&student=11366373
	
	@Override
	public int execute(String[] tokens, String phrase) {
//		p(phrase.substring(tokens[0].length()));
		if(tokens.length > 3){
			String username = tokens[1];
			String userid = "";//Users.getID(username);
			String startTime = tokens[2];
			String stopTime = tokens[3];
			Calendar c = DateFormat.getDateInstance().getCalendar();
			String date = tokens.length > 4 ? tokens[4] : c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH)+ "-" + c.get(Calendar.DAY_OF_MONTH);
		
		    try {
		    	WebClient webClient;
				HtmlPage page;
				{
					P p = Dexter.login();
			    	webClient = p.webClient;
			    	page = p.page;
				}
				
			    webClient.loadWebResponse(new WebRequest(new URL(""), HttpMethod.POST));
			    page = webClient.getPage("http://dexter.dagy.danderyd.se/Default.asp?page=gy/absence/absence_editnotifiedabsence");
	
			    HtmlSelect reason = page.getElementByName("reason");
			    reason.setSelectedAttribute(reason.getOption(2), true);
			    HtmlTextInput startingtime = (HtmlTextInput) page.getElementById("startingtime");
			    	startingtime.setText(startTime);
			    HtmlTextInput stoppingtime = (HtmlTextInput) page.getElementById("stopingtime");
			    	stoppingtime.setText(stopTime);
			    HtmlTextInput startdate = (HtmlTextInput) page.getElementById("startdate");
			    	startdate.setText(date);
			    HtmlTextInput stopdate = (HtmlTextInput) page.getElementById("stopdate");
			   		stopdate.setText(date);
			    HtmlElement studentid = page.getElementById("studentid");
			    	studentid.setAttribute("value", userid);//11366373
			    HtmlElement editedBy = page.getElementById("D_EditedBy");
			    	editedBy.setAttribute("value", username);//eriihr09
			    
			    HtmlButtonInput button = page.getElementByName("btnSave");
			    page = button.click();
			    
			    p(page);
			    pl(page.getTitleText() + " : " + page.getUrl().toString());
			    webClient.closeAllWindows();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			System.exit(0);
		}
		return 0;
	}
	
	void p(HtmlPage p){
		FileWriter fstream;
		try {
			fstream = new FileWriter("a.html");
		    BufferedWriter out = new BufferedWriter(fstream);
		    out.write(p.asText());
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void f(List<String> l, String name){
		FileWriter fstream;
		try {
			fstream = new FileWriter(name);
		    BufferedWriter out = new BufferedWriter(fstream);
		    for(String s : l){
		    	 out.write(s + "\n");
		    }
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static class P {
		public WebClient webClient;
		public HtmlPage page;
		
		public P(WebClient webClient, HtmlPage page){
			this.webClient = webClient;
			this.page = page;
		}
	}
	
	public static P login(){
		return login(DEFAULT_USERNAME, DEFAULT_PASSWORD);
	}
	
	public static P login(String username, String password){
		try {
			WebClient webClient = new WebClient(BrowserVersion.INTERNET_EXPLORER_8);
	    	webClient.setRefreshHandler(new ThreadedRefreshHandler());
			HtmlPage page = webClient.getPage("http://dexter.dagy.danderyd.se/Default.asp?page=auth/common/login");
			
		    HtmlForm form = page.getFormByName("frmLoginDexter");
	
		    HtmlTextInput user = form.getInputByName("username");
		    user.setValueAttribute(username);
		    HtmlPasswordInput pw = form.getInputByName("password");
		    pw.setValueAttribute(password);
		    
		    HtmlButtonInput button = page.getElementByName("btnLogInDexter");//form.getInputByName("btnLogInDexter");
	    
			page = button.click();
			
//		    HtmlDivision div = page.getHtmlElementById("clsTextError");
//		    pl(div.asText());
			
			 if(page.getTitleText().contains("Startsida")){
		    	return new P(webClient, page);
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}


