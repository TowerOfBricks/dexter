package se.exuvo.dexter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Users {
	static List<User> users = new ArrayList<User>();
	
	
	public static synchronized User getUser(String username){
		for(User u : users){
			if(u.username.equals(username)){
				return u;
			}
		}
		return null;
	}
	
	public static synchronized void save(){
		FileWriter fstream;
		try {
			fstream = new FileWriter("users.txt");
		    BufferedWriter out = new BufferedWriter(fstream);
//		    Collections.sort(users, sorter);
		    for(User u : users){
		    	 out.write(u.username + "," + u.id + "," + u.name + "," + u.klass + "\n");
		    }
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void load(){
		FileReader fr;
		try{
			fr = new FileReader("users.txt");
			BufferedReader in = new BufferedReader(fr);
			while(in.ready()){
				String s = in.readLine();
				String[] a = s.split(",");
				User u = new User();
				try{
					u.username = a[0];
					u.id = Integer.parseInt(a[1]);
					u.name = a[2];
					u.klass = a[3];
				}catch(Throwable t){
					t.printStackTrace();
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	static class Sorter implements Comparator<User>{

		@Override
		public int compare(User o1, User o2) {
			return o1.username.compareToIgnoreCase(o2.username);
		}
		
	}
	
	public static class User{
		public String username;
		public String name;
		public int id;
		public String klass;
		public String lastname;
		
		public String username(){
			return name.substring(0,3) + lastname.substring(0, 3) + klass.substring(beginIndex);
		}
	}

}
