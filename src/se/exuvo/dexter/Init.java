package se.exuvo.dexter;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import se.exuvo.dexter.commands.Command;
import se.exuvo.dexter.commands.Parser;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;

public class Init {
	protected static final Logger log = Logger.getLogger(Init.class);
	public static final long serialVersionUID = 1;;
	public static JSAPResult config = null;
	public static ShutDownHook shutdownhook = new ShutDownHook();
	
	public static void main(String[] args) {
		DOMConfigurator.configure("log4j.xml");
		log.fatal("### Starting ###");
		
		JSAP jsap = new JSAP();
        arguments(jsap);
        
        config = jsap.parse(args);
        // check whether the command line was valid, and if it wasn't, display usage information and exit.
        if (!config.success()) {
            System.err.println();
         // print out specific error messages describing the problems
            // with the command line, THEN print usage, THEN print full
            // help.  This is called "beating the user with a clue stick."
            for (Iterator<?> errs = config.getErrorMessageIterator();
                    errs.hasNext();) {
                System.err.println("Error: " + errs.next());
            }
            
            System.err.println();
            System.err.println("Usage: java "
                                + Init.class.getName());
            System.err.println("                "
                                + jsap.getUsage());
            System.err.println();
            // show full help as well
            System.err.println(jsap.getHelp());
            System.exit(1);
        }
        
        new Settings(config);
        Runtime.getRuntime().addShutdownHook(shutdownhook);
        Parser.init();
        
//        if(config.getBoolean("console")){
	    	new Thread(){
	    		public void run(){
	            	String a;
	            	Scanner s = new Scanner(System.in);
	            	Command.setOut(System.out);
	            	Command.setIn(System.in);
	            	Parser.parse("dexter");
	        		while(true){
	        			System.out.print("$ ");
	        			a = s.nextLine();
//	        			System.out.print();
	        			Parser.parse(a);
	        		}
	    		}
	    	}.start();
//        }
	}
	
	private static final void arguments(JSAP jsap){
		
		Switch console = new Switch("console")
		.setShortFlag('c')
		.setLongFlag("console");
		console.setHelp("Run in a console.");
		
		try {
			jsap.registerParameter(console);
		} catch (JSAPException e) {
			log.warn("JSAP: Failed to register parameters due to: " + e);
		}
	}

}

class ShutDownHook extends Thread{
	
	public ShutDownHook(){
		
	}
	
	public void run(){
		try{
			Settings.save();
		}catch(Throwable ignore){
			
		}
	}
}

